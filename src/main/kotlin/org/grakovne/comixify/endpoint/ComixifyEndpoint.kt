package org.grakovne.comixify.endpoint

import org.grakovne.comixify.service.ComixifyService
import org.grakovne.comixify.service.DownloadFileService
import org.springframework.core.io.InputStreamResource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Files
import java.util.*

@RestController
@RequestMapping("/api/v1/comixify")
class ComixifyEndpoint(private val comixifyService: ComixifyService,
                       private val downloadFileService: DownloadFileService) {

    @PostMapping
    fun comixify(@RequestParam("file") file: MultipartFile): ResponseEntity<InputStreamResource> {
        val tempFile = Files.createTempFile(UUID.randomUUID().toString(), ".jpg")
        file.transferTo(tempFile)

        val result = comixifyService.comixify(tempFile.toFile())

        return ResponseEntity.ok()
                .contentLength(result.length())
                .contentType(MediaType.IMAGE_JPEG)
                .body(InputStreamResource(result.inputStream()))
    }

    @GetMapping("cat")
    fun comixifyCat(): ResponseEntity<InputStreamResource> {
        val result = comixifyService.comixifyCat()

        return ResponseEntity.ok()
                .contentLength(result.length())
                .contentType(MediaType.IMAGE_JPEG)
                .body(InputStreamResource(result.inputStream()))
    }
}