package org.grakovne.comixify.endpoint

import org.grakovne.comixify.service.PublishService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/publish")
class PublishEndpoint(
        private val publishService: PublishService
){
    @PostMapping
    fun publish() {
        publishService.publishToVk()
    }
}