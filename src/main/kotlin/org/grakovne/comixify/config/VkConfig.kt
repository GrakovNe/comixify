package org.grakovne.comixify.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "vk")
class VkConfig{
    var userAccessToken: String? = null
    var groupId: Int? = null
}