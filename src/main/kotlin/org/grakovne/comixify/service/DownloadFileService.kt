package org.grakovne.comixify.service

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import java.io.File
import java.nio.file.Files
import java.util.*

@Service
class DownloadFileService(private val restTemplateBuilder: RestTemplateBuilder) {

    fun downloadFile(url: String): File {
        return makeRequest(0) {
            val array = restTemplateBuilder.build().getForEntity(url, ByteArray::class.java).body

            val path = Files.createTempFile(UUID.randomUUID().toString(), ".jpg")
            Files.write(path, array)

            path.toFile()
        }
    }

    private fun <T> makeRequest(tryNumber: Int, request: () -> T): T {
        return try {
            request.invoke()
        } catch (ex: HttpClientErrorException) {
            if (tryNumber > 60) {
                throw RuntimeException("Unable to make request")
            }

            Thread.sleep(1000)
            makeRequest(tryNumber + 1, request)
        }
    }
}