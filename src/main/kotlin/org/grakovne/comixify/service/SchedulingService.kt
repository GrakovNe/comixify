package org.grakovne.comixify.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class SchedulingService(
        val publishService: PublishService
){
    private val logger: Logger = LoggerFactory.getLogger(SchedulingService::class.java)

    @Scheduled(cron = "0 0 */2 * * *")
    fun postCat() {
        logger.info("Posting a cat")
        publishService.publishToVk()
    }
}