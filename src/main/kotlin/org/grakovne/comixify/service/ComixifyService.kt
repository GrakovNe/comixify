package org.grakovne.comixify.service

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.HttpClientErrorException
import java.io.File
import java.nio.file.Files
import java.util.*
import kotlin.collections.HashMap


@Service
class ComixifyService(
        private val restTemplateBuilder: RestTemplateBuilder,
        private val downloadFileService: DownloadFileService
) {

    fun comixifyCat(): File {
        val file = downloadFileService.downloadFile("http://theoldreader.com/kittens/1200/800/")
        return comixify(file)
    }

    fun comixify(file: File): File {

        val requestId = uploadPhoto(file)
        val meta = getMeta(requestId)
        val restyled = restyle(meta, getRandomStyle())

        return getRestyled(restyled)
    }

    private fun getRandomStyle(): String {
        return "11" // i like 11
    }

    private fun uploadPhoto(file: File?): String {
        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA

        val fileMap: MultiValueMap<String, String> = LinkedMultiValueMap()

        val contentDisposition = ContentDisposition
                .builder("form-data")
                .name("files")
                .filename("file.jpg")
                .build()
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString())
        val fileEntity = HttpEntity(Files.readAllBytes(file?.toPath()), fileMap)

        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("files", fileEntity)
        body.add("public", "true")

        val map = restTemplateBuilder
                .build()
                .postForEntity("https://comixify.ai/comixify/from_images/", body, Map::class.java).body!!

        return map["request_id"] as String
    }

    private fun getMeta(requestId: String): String {
        val raw = makeRequest(0) { restTemplateBuilder.build().getForEntity("https://comixify.ai/comixify/comics/$requestId", Map::class.java).body }
        return ((raw?.get("meta") as Map<*, *>)["id"] as Int).toString()
    }

    private fun restyle(id: String, mode: String): String {
        val data = HashMap<String, Any>()

        data["style_transfer_mode"] = mode
        data["public"] = true
        data["meta_comics_id"] = id

        val raw = makeRequest(0) { restTemplateBuilder.build().postForEntity("https://comixify.ai/comixify/restylize_comics/", data, Map::class.java).body }
        return raw?.get("request_id") as String
    }

    private fun getRestyled(id: String): File {
        return makeRequest(0) {
            val array = restTemplateBuilder.build().getForEntity("https://comixify.ai/media/frames/${id}_1.jpg", ByteArray::class.java).body

            val path = Files.createTempFile(UUID.randomUUID().toString(), ".jpg")
            Files.write(path, array)

            path.toFile()
        }
    }

    private fun <T> makeRequest(tryNumber: Int, request: () -> T): T {
        return try {
            request.invoke()
        } catch (ex: HttpClientErrorException) {
            if (tryNumber > 60) {
                throw RuntimeException("Unable to make request")
            }

            Thread.sleep(1000)
            makeRequest(tryNumber + 1, request)
        }
    }

}