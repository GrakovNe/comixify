package org.grakovne.comixify.service

import com.vk.api.sdk.client.TransportClient
import com.vk.api.sdk.client.VkApiClient
import com.vk.api.sdk.client.actors.UserActor
import com.vk.api.sdk.httpclient.HttpTransportClient
import com.vk.api.sdk.objects.photos.Photo
import com.vk.api.sdk.objects.wall.responses.PostResponse
import org.grakovne.comixify.config.VkConfig
import org.springframework.stereotype.Service
import java.io.File


@Service
class PublishService(
        private val comixifyService: ComixifyService,
        private val vkConfig: VkConfig
) {
    fun publishToVk(): PostResponse {
        val file: File = comixifyService.comixifyCat()
        val vk = getVkClient()
        val actor = UserActor(vkConfig.groupId, vkConfig.userAccessToken)

        val photo: Photo = uploadPhoto(vk, file, actor)
        return publish(vk, photo, actor)
    }

    private fun getVkClient(): VkApiClient {
        var transportClient: TransportClient = HttpTransportClient.getInstance()
        return VkApiClient(transportClient)
    }

    private fun uploadPhoto(vk: VkApiClient, file:File, actor: UserActor): Photo {
        val serverResponse = vk.photos().getWallUploadServer(actor).execute()
        val uploadResponse = vk.upload().photoWall(serverResponse.uploadUrl.toString(), file).execute()
        val photoList = vk.photos().saveWallPhoto(actor, uploadResponse.photo)
                .server(uploadResponse.server)
                .hash(uploadResponse.hash)
                .execute()

        return photoList[0]
    }

    private fun publish(vk: VkApiClient, photo: Photo, actor: UserActor): PostResponse {
        val attachId = "photo" + photo.ownerId + "_" + photo.id
        return vk.wall().post(actor)
                .attachments(attachId)
                .ownerId(-vkConfig.groupId!!)
                .execute()
    }
}